package jattra.util;

import com.google.common.io.Resources;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

public class ResourceUtil {

    public static List<String> loadResource(Class<?> contextClass, String resourceName) {
        try {
            return Resources.readLines(Resources.getResource(contextClass, resourceName), Charset.defaultCharset());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
