package jattra.danek;

import java.nio.file.Path;

public class DanekMain {

    public static void main(String[] args) {
        Path schwabExport = Path.of(args[0]);
        int year = Integer.parseInt(args[1]);
        Danek.create(year).run(schwabExport);
    }

}
