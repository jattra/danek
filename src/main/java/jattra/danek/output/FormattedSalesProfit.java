package jattra.danek.output;

import lombok.Value;

import java.util.List;

@Value
public class FormattedSalesProfit {

    List<FormattedSale> sales;
    String sumIncome;
    String sumCosts;
    String taxableProfit;
}
