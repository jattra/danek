package jattra.danek.output;

import com.google.common.collect.ImmutableList;
import jattra.danek.compute.ConvertedRsu;
import jattra.danek.compute.RsuTotalIncome;
import jattra.danek.compute.YearExchangeRate;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class RsuIncomePrinter {

    private final FormattingHelper format = new FormattingHelper();
    private final YearExchangeRate yearExchangeRate;


    public void print(RsuTotalIncome rsuIncome) {
        List<FormattedRsu> rsuLines = format(rsuIncome);

        new PrintableTitle("RS").print();

        new PrintableTable()
                .header(
                        "datum připsání",
                        "počet akcií",
                        "tržní cena (USD)",
                        "příjem (USD)",
                        "příjem (Kč)*"
                )
                .addRows(
                        rsuLines,
                        rsu -> List.of(
                                rsu.vestDate(),
                                rsu.quantity(),
                                rsu.vestFvm(),
                                rsu.income(),
                                rsu.czkIncome()
                        ))
                .addFooter("*) Přepočteno kurzem dle " + format.formatGfr(yearExchangeRate))
                .print();
    }

    private ImmutableList<FormattedRsu> format(RsuTotalIncome rsuIncome) {
        List<FormattedRsu> formattedRsus = rsuIncome.rsuIncomes()
                .stream()
                .map(this::format)
                .toList();

        return ImmutableList.<FormattedRsu>builder()
                .addAll(formattedRsus)
                .add(new FormattedRsu(
                        "celkem",
                        String.valueOf(rsuIncome.sumQuantity()),
                        "",
                        format.formatPrice(rsuIncome.sumIncome()),
                        format.formatPrice(rsuIncome.czkSumIncome())
                ))
                .build();
    }

    private FormattedRsu format(ConvertedRsu rsu) {
        return new FormattedRsu(
                format.formatDate(rsu.vestDate()),
                String.valueOf(rsu.quantity()),
                format.formatPrice(rsu.vestFmv()),
                format.formatPrice(rsu.income()),
                format.formatPrice(rsu.czkIncome())
        );
    }
}
