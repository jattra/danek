package jattra.danek.output;

import com.google.common.collect.ImmutableList;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class PrintableTable {

    private static final String DELIMITER = "\t";

    private List<String> header;
    private final List<Row> rows = new ArrayList<>();
    private final List<String> footer = new ArrayList<>();


    public void print() {
        System.out.println(String.join(DELIMITER, header));
        rows.forEach(row -> System.out.println(String.join(DELIMITER, row.cells)));
        footer.forEach(System.out::println);
        System.out.println("\n");
    }

    public PrintableTable header(String... header) {
        this.header = ImmutableList.copyOf(header);
        return this;
    }

    public PrintableTable addRow(String... cells) {
        return addRow(Arrays.asList(cells));
    }

    @SuppressWarnings("WeakerAccess")
    public PrintableTable addRow(List<String> cells) {
        rows.add(new Row(cells));
        return this;
    }

    public <T> PrintableTable addRows(List<T> objects, Function<T, List<String>> valueExtractor) {
        objects.forEach(o -> addRow(valueExtractor.apply(o)));
        return this;
    }

    public PrintableTable addFooter(String footer) {
        this.footer.add(footer);
        return this;
    }

    @RequiredArgsConstructor
    private static class Row {
        private final List<String> cells;
    }
}
