package jattra.danek.output;

import com.google.common.collect.ImmutableList;
import jattra.danek.compute.ConvertedEspp;
import jattra.danek.compute.EsppTotalIncome;
import jattra.danek.compute.YearExchangeRate;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class EsppIncomePrinter {

    private final FormattingHelper format = new FormattingHelper();
    private final YearExchangeRate yearExchangeRate;


    public void print(EsppTotalIncome esppIncome) {
        List<FormattedEspp> esppLines = format(esppIncome);

        new PrintableTitle("ESPP").print();

        new PrintableTable()
                .header(
                        "datum nákupu",
                        "počet akcií",
                        "zvýhodněná nákupní cena (USD)",
                        "tržní cena (USD)",
                        "zisk (USD)",
                        "zisk (Kč)*"
                )
                .addRows(
                        esppLines,
                        espp -> List.of(
                                espp.purchaseDate(),
                                espp.quantity(),
                                espp.purchasePrice(),
                                espp.fmvPrice(),
                                espp.income(),
                                espp.czkIncome()
                        ))
                .addFooter("*) Přepočteno kurzem dle " + format.formatGfr(yearExchangeRate))
                .print();
    }

    private ImmutableList<FormattedEspp> format(EsppTotalIncome esppIncome) {
        List<FormattedEspp> formattedEspp = esppIncome.espps()
                .stream()
                .map(this::format)
                .toList();

        return ImmutableList.<FormattedEspp>builder()
                .addAll(formattedEspp)
                .add(new FormattedEspp(
                        "celkem",
                        String.valueOf(esppIncome.sumQuantity()),
                        "",
                        "",
                        format.formatPrice(esppIncome.sumIncome()),
                        format.formatPrice(esppIncome.czkSumIncome())
                ))
                .build();
    }

    private FormattedEspp format(ConvertedEspp espp) {
        return new FormattedEspp(
                format.formatDate(espp.purchaseDate()),
                String.valueOf(espp.quantity()),
                format.formatPrice(espp.purchasePrice()),
                format.formatPrice(espp.fmvPrice()),
                format.formatPrice(espp.income()),
                format.formatPrice(espp.czkIncome())
        );
    }
}
