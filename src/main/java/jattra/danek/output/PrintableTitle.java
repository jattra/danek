package jattra.danek.output;

public record PrintableTitle(String title) {

    public void print() {
        System.out.println("\n*** " + title + " ***\n");
    }
}
