package jattra.danek.output;

public record FormattedRsu(
        String vestDate,
        String quantity,
        String vestFvm,
        String income,
        String czkIncome
) {
}
