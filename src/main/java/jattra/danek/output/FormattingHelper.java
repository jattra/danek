package jattra.danek.output;

import jattra.danek.compute.YearExchangeRate;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;

public class FormattingHelper {

    private final NumberFormat priceFormat;
    private final DateTimeFormatter dateTimeFormat;

    public FormattingHelper() {
        DecimalFormatSymbols formatSymbols = new DecimalFormatSymbols();
        formatSymbols.setDecimalSeparator(',');
        priceFormat = new DecimalFormat("#.00", formatSymbols);
        dateTimeFormat = new DateTimeFormatterBuilder().appendPattern("dd.MM.yyyy").toFormatter();
    }

    public String formatGfr(YearExchangeRate rate) {
        return String.format("GFŘ - %s: %s Kč/USD", rate.gfr(), formatConversionRate(rate.rate()));
    }

    public String formatPrice(double d) {
        return priceFormat.format(d);
    }

    public String formatConversionRate(double d) {
        return String.valueOf(d).replace('.', ',');
    }

    public String formatDate(LocalDate date) {
        return dateTimeFormat.format(date);
    }
}
