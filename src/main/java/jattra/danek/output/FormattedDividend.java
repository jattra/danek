package jattra.danek.output;

public record FormattedDividend(
        String date,
        String amount,
        String tax,
        String czkAmount,
        String czkTax
) {
}
