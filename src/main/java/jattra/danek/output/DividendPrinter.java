package jattra.danek.output;

import jattra.danek.compute.ConvertedDividend;
import jattra.danek.compute.DividendSummary;
import jattra.danek.compute.YearExchangeRate;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class DividendPrinter {

    private final FormattingHelper format = new FormattingHelper();
    private final YearExchangeRate yearExchangeRate;

    public void print(DividendSummary summary) {
        List<FormattedDividend> formattedDividends = summary.dividends()
                .stream()
                .map(this::format)
                .toList();

        new PrintableTitle("Dividendy").print();

        new PrintableTable()
                .header(
                        "země původu",
                        "datum",
                        "dividenda (USD)",
                        "sražená daň (USD)",
                        "dividenda (Kč)*",
                        "sražená daň (Kč)*"
                )
                .addRows(
                        formattedDividends,
                        d -> List.of(
                                "US",
                                d.date(),
                                d.amount(),
                                d.tax(),
                                d.czkAmount(),
                                d.czkTax()
                        ))
                .addRow(
                        "celkem",
                        "",
                        format.formatPrice(summary.totalDividend()),
                        format.formatPrice(summary.totalTax()),
                        format.formatPrice(summary.czkTotalDividend()),
                        format.formatPrice(summary.czkTotalTax()))
                .addFooter("*) Přepočteno kurzem dle " + format.formatGfr(yearExchangeRate))
                .print();
    }

    private FormattedDividend format(ConvertedDividend dividend) {
        return new FormattedDividend(
                format.formatDate(dividend.date()),
                format.formatPrice(dividend.amount()),
                format.formatPrice(dividend.tax()),
                format.formatPrice(dividend.czkAmount()),
                format.formatPrice(dividend.czkTax())
        );
    }
}
