package jattra.danek.output;

import com.google.common.collect.ImmutableList;
import jattra.danek.compute.ConvertedSale;
import jattra.danek.compute.SalesProfit;
import jattra.danek.compute.YearExchangeRate;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class SalesProfitPrinter {

    private final FormattingHelper format = new FormattingHelper();
    private final YearExchangeRate yearExchangeRate;


    public void print(SalesProfit salesProfit) {
        FormattedSalesProfit formattedProfit = format(salesProfit);

        new PrintableTitle("Prodej akcií").print();

        new PrintableTable()
                .header(
                        "datum prodeje",
                        "datum nákupu",
                        "podléhá zdanění*",
                        "počet akcií",
                        "prodejní cena (USD)",
                        "nákupní cena (USD)",
                        "nákupní kurz (Kč/USD)",
                        "příjem (Kč)**",
                        "výdaje (Kč)***"
                )
                .addRows(
                        formattedProfit.getSales(),
                        s -> List.of(
                                s.getSaleDate(),
                                s.getPurchaseDate(),
                                s.getTaxableFlag(),
                                s.getQuantity(),
                                s.getSalePrice(),
                                s.getPurchasePrice(),
                                s.getPurchaseConversionRate(),
                                s.getIncome(),
                                s.getCosts()
                        ))
                .addFooter("*) Zisky splňující tříletý časový test nepodléhají zdanění.")
                .addFooter("**) Přepočteno kurzem dle " + format.formatGfr(yearExchangeRate))
                .addFooter("***) Přepočteno nákupním kurzem pro daný rok dle GFŘ.")
                .print();

        new PrintableTable()
                .header("celkové příjmy (Kč)", "celkové výdaje (Kč)", "celkový zisk (Kč)")
                .addRow(formattedProfit.getSumIncome(), formattedProfit.getSumCosts(), formattedProfit.getTaxableProfit())
                .print();
    }

    private FormattedSalesProfit format(SalesProfit profit) {
        List<FormattedSale> sales = profit.sales()
                .stream()
                .map(this::format)
                .collect(Collectors.toList());

        return new FormattedSalesProfit(
                ImmutableList.copyOf(sales),
                format.formatPrice(profit.sumIncome()),
                format.formatPrice(profit.sumCosts()),
                format.formatPrice(profit.taxableProfit())
        );
    }

    private FormattedSale format(ConvertedSale s) {
        return new FormattedSale(
                format.formatDate(s.saleDate()),
                format.formatDate(s.purchaseDate()),
                s.isTaxable() ? "ANO" : "NE",
                String.valueOf(s.quantity()),
                format.formatPrice(s.salePrice()),
                format.formatPrice(s.purchasePrice()),
                format.formatConversionRate(s.purchaseConversionRate()),
                format.formatPrice(s.getIncome()),
                format.formatPrice(s.getCosts())
        );
    }
}
