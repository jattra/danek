package jattra.danek.output;

import lombok.Value;

@Value
public class FormattedSale {
    String saleDate;
    String purchaseDate;
    String taxableFlag;
    String quantity;
    String salePrice;
    String purchasePrice;
    String purchaseConversionRate;
    String income;
    String costs;
}
