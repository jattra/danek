package jattra.danek.output;

public record FormattedEspp(
        String purchaseDate,
        String quantity,
        String purchasePrice,
        String fmvPrice,
        String income,
        String czkIncome
) {
}
