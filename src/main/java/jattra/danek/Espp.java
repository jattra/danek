package jattra.danek;

import jattra.danek.compute.EsppIncomeComputer;
import jattra.danek.compute.EsppTotalIncome;
import jattra.danek.input.ParsedInput;
import jattra.danek.output.EsppIncomePrinter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class Espp {

    private final EsppIncomeComputer computer;
    private final EsppIncomePrinter printer;

    public void doEspp(ParsedInput parsedInput) {
        EsppTotalIncome income = computer.compute(parsedInput.getEsppRecords());
        printer.print(income);
    }
}
