package jattra.danek;

import jattra.danek.compute.DividendComputer;
import jattra.danek.compute.DividendSummary;
import jattra.danek.input.ParsedInput;
import jattra.danek.output.DividendPrinter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class Dividends {

    private final DividendComputer computer;
    private final DividendPrinter printer;

    public void doDividends(ParsedInput input) {
        DividendSummary dividends = computer.compute(input.getDividendRecords(), input.getTaxRecords());
        printer.print(dividends);
    }
}
