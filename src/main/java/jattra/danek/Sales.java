package jattra.danek;

import jattra.danek.compute.SalesMerger;
import jattra.danek.compute.SalesProfit;
import jattra.danek.compute.SalesProfitComputer;
import jattra.danek.input.ParsedInput;
import jattra.danek.input.SaleRecord;
import jattra.danek.output.SalesProfitPrinter;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class Sales {
    private final SalesMerger salesMerger;
    private final SalesProfitComputer salesProfitComputer;
    private final SalesProfitPrinter salesProfitPrinter;

    public void doSales(ParsedInput parsedInput) {
        List<SaleRecord> merged = salesMerger.merge(parsedInput.getSaleRecords());
        SalesProfit salesProfit = salesProfitComputer.computeTax(merged);
        salesProfitPrinter.print(salesProfit);
    }
}
