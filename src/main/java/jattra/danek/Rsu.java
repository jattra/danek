package jattra.danek;

import jattra.danek.compute.RsuIncomeComputer;
import jattra.danek.compute.RsuTotalIncome;
import jattra.danek.input.ParsedInput;
import jattra.danek.output.RsuIncomePrinter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class Rsu {

    private final RsuIncomeComputer rsuIncomeComputer;
    private final RsuIncomePrinter rsuIncomePrinter;

    public void doRsu(ParsedInput parsedInput) {
        RsuTotalIncome income = rsuIncomeComputer.compute(parsedInput.getRsuRecords());
        rsuIncomePrinter.print(income);
    }
}
