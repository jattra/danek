package jattra.danek;

import jattra.danek.compute.*;
import jattra.danek.input.InputParser;
import jattra.danek.input.ParsedInput;
import jattra.danek.input.YearFilter;
import jattra.danek.output.DividendPrinter;
import jattra.danek.output.EsppIncomePrinter;
import jattra.danek.output.RsuIncomePrinter;
import jattra.danek.output.SalesProfitPrinter;
import lombok.RequiredArgsConstructor;

import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDate;

@RequiredArgsConstructor
public class Danek {

    private final InputParser inputParser;
    private final YearFilter yearFilter;
    private final Sales sales;
    private final Dividends dividends;
    private final Espp espp;
    private final Rsu rsu;


    public static Danek create(int year) {
        YearConstantExchangeRateProvider exchangeRateProvider = YearConstantExchangeRateProvider.hardcoded();
        YearExchangeRate yearExchangeRate = exchangeRateProvider.rateAt(LocalDate.of(year, 1, 1));
        return new Danek(
                new InputParser(),
                new YearFilter(year),
                new Sales(
                        new SalesMerger(),
                        new SalesProfitComputer(exchangeRateProvider),
                        new SalesProfitPrinter(yearExchangeRate)
                ),
                new Dividends(
                        new DividendComputer(exchangeRateProvider),
                        new DividendPrinter(yearExchangeRate)
                ),
                new Espp(
                        new EsppIncomeComputer(exchangeRateProvider),
                        new EsppIncomePrinter(yearExchangeRate)
                ),
                new Rsu(
                        new RsuIncomeComputer(exchangeRateProvider),
                        new RsuIncomePrinter(yearExchangeRate)
                )
        );
    }

    public void run(Path schwabExportFile) {
        try {
            ParsedInput parsedInput = yearFilter.filterThatYear(inputParser.parse(schwabExportFile));
            sales.doSales(parsedInput);
            dividends.doDividends(parsedInput);
            espp.doEspp(parsedInput);
            rsu.doRsu(parsedInput);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
