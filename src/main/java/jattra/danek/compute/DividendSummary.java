package jattra.danek.compute;

import java.util.List;

public record DividendSummary(
        List<ConvertedDividend> dividends,
        double totalDividend,
        double totalTax,
        double czkTotalDividend,
        double czkTotalTax
) {
}
