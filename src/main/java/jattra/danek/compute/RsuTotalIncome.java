package jattra.danek.compute;

import java.util.List;

public record RsuTotalIncome(
        List<ConvertedRsu> rsuIncomes,
        int sumQuantity,
        double sumIncome,
        double czkSumIncome
) {
}
