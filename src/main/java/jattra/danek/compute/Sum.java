package jattra.danek.compute;

import java.util.List;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;

public class Sum {

    public static <T> double sumPrice(List<T> items, ToDoubleFunction<T> priceGetter) {
        return items
                .stream()
                .mapToDouble(priceGetter)
                .sum();
    }

    public static <T> int sumQuantity(List<T> items, ToIntFunction<T> quantityGetter) {
        return items
                .stream()
                .mapToInt(quantityGetter)
                .sum();
    }
}
