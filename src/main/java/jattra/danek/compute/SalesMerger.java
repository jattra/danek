package jattra.danek.compute;

import jattra.danek.input.SaleRecord;

import java.time.LocalDate;
import java.util.List;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingInt;

public class SalesMerger {

    public List<SaleRecord> merge(List<SaleRecord> records) {
        return records.stream()
                .collect(groupingBy(Key::from, summingInt(SaleRecord::getQuantity)))
                .entrySet()
                .stream()
                .map(e -> e.getKey().createSaleRecord(e.getValue()))
                .toList();
    }

    private record Key(
            LocalDate saleDate,
            double salePrice,
            double purchasePrice,
            LocalDate purchaseDate
    ) {
        public static Key from(SaleRecord record) {
            return new Key(
                    record.getSaleDate(),
                    record.getSalePrice(),
                    record.getPurchasePrice(),
                    record.getPurchaseDate()
            );
        }

        public SaleRecord createSaleRecord(Integer quantity) {
            return new SaleRecord(
                    saleDate,
                    quantity,
                    salePrice,
                    purchasePrice,
                    purchaseDate
            );
        }
    }
}
