package jattra.danek.compute;

public record YearExchangeRate(int year, double rate, String gfr) {
}
