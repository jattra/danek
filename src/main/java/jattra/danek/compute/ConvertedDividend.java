package jattra.danek.compute;

import java.time.LocalDate;

public record ConvertedDividend(
        LocalDate date,
        double amount,
        double tax,
        double czkAmount,
        double czkTax
) {
}
