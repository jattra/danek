package jattra.danek.compute;

import com.google.common.collect.Maps;
import jattra.danek.input.DividendRecord;
import jattra.danek.input.TaxRecord;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

@RequiredArgsConstructor
public class DividendComputer {

    private final ExchangeRateProvider exchangeRateProvider;


    public DividendSummary compute(
            List<DividendRecord> dividends,
            List<TaxRecord> taxes
    ) {
        Map<LocalDate, TaxRecord> taxRecords = Maps.uniqueIndex(taxes, TaxRecord::getDate);

        List<ConvertedDividend> convertedDividends = dividends
                .stream()
                .map(d -> convert(d, taxRecords.get(d.getDate())))
                .sorted(Comparator.comparing(ConvertedDividend::date))
                .toList();

        double sumDividend = Sum.sumPrice(convertedDividends, ConvertedDividend::amount);
        double czkSumDividend = Sum.sumPrice(convertedDividends, ConvertedDividend::czkAmount);
        double sumTax = Sum.sumPrice(convertedDividends, ConvertedDividend::tax);
        double czkSumTax = Sum.sumPrice(convertedDividends, ConvertedDividend::czkTax);

        return new DividendSummary(
                convertedDividends,
                sumDividend,
                sumTax,
                czkSumDividend,
                czkSumTax
        );
    }

    private ConvertedDividend convert(DividendRecord dividendRecord, @Nullable TaxRecord taxRecord) {
        double tax = checkNotNull(taxRecord).getAmount() * -1;
        double rate = exchangeRateProvider.rateAt(dividendRecord.getDate()).rate();
        return new ConvertedDividend(
                dividendRecord.getDate(),
                dividendRecord.getAmount(),
                tax,
                rate * dividendRecord.getAmount(),
                rate * tax
        );
    }
}
