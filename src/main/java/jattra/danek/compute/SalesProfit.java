package jattra.danek.compute;

import java.util.List;

public record SalesProfit(
        List<ConvertedSale> sales,
        double sumIncome,
        double sumCosts,
        double taxableProfit
) {
}
