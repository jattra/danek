package jattra.danek.compute;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import jattra.danek.input.RsuRecord;
import lombok.RequiredArgsConstructor;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class RsuIncomeComputer {

    private final ExchangeRateProvider exchangeRateProvider;

    public RsuTotalIncome compute(List<RsuRecord> rsuRecords) {
        List<ConvertedRsu> sortedRsu = rsuRecords
                .stream()
                .collect(Collectors.groupingBy(RsuRecord::getVestDate))
                .values().stream()
                .map(this::convert)
                .sorted(Comparator.comparing(ConvertedRsu::vestDate))
                .toList();

        int sumQuantity = Sum.sumQuantity(sortedRsu, ConvertedRsu::quantity);
        double sumIncome = Sum.sumPrice(sortedRsu, ConvertedRsu::income);
        double czkSumIncome = Sum.sumPrice(sortedRsu, ConvertedRsu::czkIncome);

        return new RsuTotalIncome(
                sortedRsu,
                sumQuantity,
                sumIncome,
                czkSumIncome
        );
    }

    private ConvertedRsu convert(List<RsuRecord> rsuRecords) {
        RsuRecord rsuRecord = new RsuRecord(
                checkSameAndGet(rsuRecords, RsuRecord::getDate),
                rsuRecords.stream().mapToInt(RsuRecord::getQuantity).sum(),
                checkSameAndGet(rsuRecords, RsuRecord::getVestFmv),
                checkSameAndGet(rsuRecords, RsuRecord::getVestDate)
        );

        double income = rsuRecord.getQuantity() * rsuRecord.getVestFmv();
        double rate = exchangeRateProvider.rateAt(rsuRecord.getVestDate()).rate();
        return new ConvertedRsu(
                rsuRecord.getVestDate(),
                rsuRecord.getQuantity(),
                rsuRecord.getVestFmv(),
                income,
                rate * income
        );
    }

    private <T> T checkSameAndGet(List<RsuRecord> records, Function<RsuRecord, T> getter) {
        Set<T> values = records.stream().map(getter).collect(Collectors.toSet());
        Preconditions.checkArgument(values.size() == 1);
        return values.iterator().next();
    }
}
