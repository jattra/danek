package jattra.danek.compute;

import java.time.LocalDate;

public record ConvertedEspp(
        LocalDate purchaseDate,
        int quantity,
        double purchasePrice,
        double fmvPrice,
        double income,
        double czkIncome
) {
}
