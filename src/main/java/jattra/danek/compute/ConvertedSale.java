package jattra.danek.compute;

import java.time.LocalDate;

public record ConvertedSale(
        LocalDate saleDate,
        int quantity,
        double salePrice,
        double purchasePrice,
        LocalDate purchaseDate,
        double purchaseConversionRate,
        double czkPurchasePrice,
        double czkSalePrice
) {
    public double getIncome() {
        return czkSalePrice * quantity();
    }

    public double getCosts() {
        return czkPurchasePrice * quantity();
    }

    public boolean isTaxable() {
        return saleDate.isBefore(purchaseDate.plusYears(3));
    }
}
