package jattra.danek.compute;

import jattra.danek.input.SaleRecord;
import lombok.RequiredArgsConstructor;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class SalesProfitComputer {

    private final ExchangeRateProvider exchangeRateProvider;

    public SalesProfit computeTax(List<SaleRecord> sales) {

        List<ConvertedSale> sortedSales = sales
                .stream()
                .filter(s -> s.getQuantity() > 0)
                .sorted(Comparator.comparing(SaleRecord::getSaleDate))
                .map(this::convert)
                .collect(Collectors.toList());

        List<ConvertedSale> taxableSales = sortedSales
                .stream()
                .filter(ConvertedSale::isTaxable)
                .collect(Collectors.toList());

        double income = Sum.sumPrice(taxableSales, ConvertedSale::getIncome);
        double costs = Sum.sumPrice(taxableSales, ConvertedSale::getCosts);

        return new SalesProfit(
                sortedSales,
                income,
                costs,
                income - costs
        );
    }

    private ConvertedSale convert(SaleRecord sale) {
        double conversionRate = exchangeRateProvider.rateAt(sale.getPurchaseDate()).rate();
        return new ConvertedSale(
                sale.getSaleDate(),
                sale.getQuantity(),
                sale.getSalePrice(),
                sale.getPurchasePrice(),
                sale.getPurchaseDate(),
                conversionRate,
                sale.getPurchasePrice() * conversionRate,
                sale.getSalePrice() * exchangeRateProvider.rateAt(sale.getSaleDate()).rate()
        );
    }
}
