package jattra.danek.compute;

import lombok.RequiredArgsConstructor;

import java.time.LocalDate;
import java.util.Map;

@RequiredArgsConstructor
public class YearConstantExchangeRateProvider implements ExchangeRateProvider {

    private final Map<Integer, YearExchangeRate> exchange;

    public static YearConstantExchangeRateProvider hardcoded() {
        return new YearConstantExchangeRateProvider(
                Map.of(
                        2018, new YearExchangeRate(2018, 21.78, ""),
                        2019, new YearExchangeRate(2019, 22.93, ""),
                        2020, new YearExchangeRate(2020, 23.14, ""),
                        2021, new YearExchangeRate(2021, 21.72, "D54"),
                        2022, new YearExchangeRate(2022, 23.41, "D60"),
                        2023, new YearExchangeRate(2023, 22.14, "D63")
                )
        );
    }

    @Override
    public YearExchangeRate rateAt(LocalDate day) {
        YearExchangeRate maybeResult = exchange.get(day.getYear());
        if (maybeResult == null) {
            throw new IllegalArgumentException("can not find rate for " + day);
        }
        return maybeResult;
    }
}
