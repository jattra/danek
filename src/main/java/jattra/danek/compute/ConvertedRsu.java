package jattra.danek.compute;

import java.time.LocalDate;

public record ConvertedRsu(
        LocalDate vestDate,
        int quantity,
        double vestFmv,
        double income,
        double czkIncome
) {
}
