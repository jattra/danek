package jattra.danek.compute;

import jattra.danek.input.EsppRecord;
import lombok.RequiredArgsConstructor;

import java.util.Comparator;
import java.util.List;

@RequiredArgsConstructor
public class EsppIncomeComputer {

    private final ExchangeRateProvider exchangeRateProvider;

    public EsppTotalIncome compute(List<EsppRecord> esppRecords) {
        List<ConvertedEspp> sortedEspp = esppRecords
                .stream()
                .map(this::convert)
                .sorted(Comparator.comparing(ConvertedEspp::purchaseDate))
                .toList();

        int sumQuantity = Sum.sumQuantity(sortedEspp, ConvertedEspp::quantity);
        double sumIncome = Sum.sumPrice(sortedEspp, ConvertedEspp::income);
        double czkSumIncome = Sum.sumPrice(sortedEspp, ConvertedEspp::czkIncome);

        return new EsppTotalIncome(
                sortedEspp,
                sumQuantity,
                sumIncome,
                czkSumIncome
        );
    }

    private ConvertedEspp convert(EsppRecord esppRecord) {
        double income = esppRecord.getQuantity() * (esppRecord.getPurchaseFmv() - esppRecord.getPurchasePrice());
        double rate = exchangeRateProvider.rateAt(esppRecord.getPurchaseDate()).rate();
        return new ConvertedEspp(
                esppRecord.getPurchaseDate(),
                esppRecord.getQuantity(),
                esppRecord.getPurchasePrice(),
                esppRecord.getPurchaseFmv(),
                income,
                rate * income
        );
    }
}
