package jattra.danek.compute;

import java.util.List;

public record EsppTotalIncome(
        List<ConvertedEspp> espps,
        int sumQuantity,
        double sumIncome,
        double czkSumIncome
) {
}
