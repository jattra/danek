package jattra.danek.compute;

import java.time.LocalDate;

public interface ExchangeRateProvider {
    YearExchangeRate rateAt(LocalDate day);
}
