package jattra.danek.input;

import java.time.LocalDate;

public interface HavingDecisiveDate {

    LocalDate getDecisiveDate();
}
