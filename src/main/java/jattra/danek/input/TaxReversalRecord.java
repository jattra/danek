package jattra.danek.input;

import lombok.Value;

import java.time.LocalDate;

@Value
public class TaxReversalRecord {
    LocalDate date;
    double amount;
}
