package jattra.danek.input;

import lombok.Value;

import java.time.LocalDate;

@Value
public class DividendRecord implements HavingDecisiveDate {
    LocalDate date;
    double amount;

    @Override
    public LocalDate getDecisiveDate() {
        return date;
    }
}
