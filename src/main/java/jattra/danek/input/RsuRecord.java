package jattra.danek.input;

import lombok.Value;

import java.time.LocalDate;

@Value
public class RsuRecord implements HavingDecisiveDate {
    LocalDate date;
    int quantity;
    double vestFmv;
    LocalDate vestDate;

    @Override
    public LocalDate getDecisiveDate() {
        return vestDate;
    }
}
