package jattra.danek.input;

import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.TemporalQueries;

public class ParsingUtil {

    private static final DateTimeFormatter DATE_FORMATTER = new DateTimeFormatterBuilder().appendPattern("MM/dd/yyyy").toFormatter();

    public static LocalDate parseDate(String text) {
        return DATE_FORMATTER .parse(text, TemporalQueries.localDate());
    }

    public static double parsePrice(String amount) {

        if (amount.startsWith("-")) {
            return -parsePositiveAmount(StringUtils.stripStart(amount, "-"));
        } else {
            return parsePositiveAmount(amount);
        }
    }

    private static double parsePositiveAmount(String amount) {
        String preprocessed = StringUtils.replace(amount, ",", "");
        return Double.parseDouble(StringUtils.stripStart(preprocessed, "$"));

    }
}
