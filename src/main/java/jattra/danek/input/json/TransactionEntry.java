package jattra.danek.input.json;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public record TransactionEntry(
        @JsonProperty(value = "Date") String date,
        @JsonProperty(value = "Action") String action,
        @JsonProperty(value = "Quantity") String quantity,
        @JsonProperty(value = "Description") String description,
        @JsonProperty(value = "Amount") String amount,
        @JsonProperty(value = "TransactionDetails") List<Inner> details
) {
    public record Inner (
            @JsonProperty(value = "Details") TransactionDetail detail
    ){}
}
