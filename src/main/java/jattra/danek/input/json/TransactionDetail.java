package jattra.danek.input.json;

import com.fasterxml.jackson.annotation.JsonProperty;

public record TransactionDetail(
            @JsonProperty(value = "Type") String type,
            @JsonProperty(value = "Shares") String shares,
            @JsonProperty(value = "SalePrice") String salePrice,
            @JsonProperty(value = "PurchaseDate") String purchaseDate,
            @JsonProperty(value = "PurchasePrice") String purchasePrice,
            @JsonProperty(value = "PurchaseFairMarketValue") String purchaseFmv,
            @JsonProperty(value = "VestDate") String vestDate,
            @JsonProperty(value = "VestFairMarketValue") String vestFmv
) {}


