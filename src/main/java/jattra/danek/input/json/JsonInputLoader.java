package jattra.danek.input.json;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;

public class JsonInputLoader {

    public final ObjectMapper mapper = new ObjectMapper().disable(FAIL_ON_UNKNOWN_PROPERTIES);

    public List<TransactionEntry> load(InputStream json) {
        try {
            return mapper.readValue(json, Json.class).Transactions;
        } catch (IOException e) {
            throw new RuntimeException("Could not load input JSON.", e);
        }
    }

    private record Json(
            List<TransactionEntry> Transactions
    ) {}
}
