package jattra.danek.input;

import jattra.danek.input.json.TransactionEntry;

import static jattra.danek.input.ParsingUtil.parseDate;
import static jattra.danek.input.ParsingUtil.parsePrice;

public class TaxJsonParser {

    public TaxRecord parse(TransactionEntry t) {
        return new TaxRecord(
                parseDate(t.date()),
                parsePrice(t.amount())
        );
    }
}
