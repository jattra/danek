package jattra.danek.input;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static jattra.danek.input.ParsingUtil.parseDate;
import static jattra.danek.input.ParsingUtil.parsePrice;

public class CsvInputParser {

    private final CsvReader objectReader;
    private final SalesParser salesParser;

    public CsvInputParser() {
        objectReader = CsvReader.SHARED;
        salesParser = new SalesParser(objectReader);
    }

    @SuppressWarnings("NestedSwitchStatement")
    public ParsedInput parse(Path inputFile) throws IOException {

        List<RsuRecord> rsuRecords = new ArrayList<>();
        List<EsppRecord> esppRecords = new ArrayList<>();
        List<DividendRecord> dividendRecords = new ArrayList<>();
        List<TaxRecord> taxRecords = new ArrayList<>();
        List<SaleRecord> saleRecords = new ArrayList<>();

        Page page = new Page(Files.readAllLines(inputFile)).skipNextLine(); // two unimportant lines

        do {
            String[] values = objectReader.readLine(page.line());

            if (!values[0].isEmpty()) {

                LocalDate date = parseDate(values[0]);
                String action = values[1];
                String description = values[3];
                String quantity = values[4];
                String amount = values[7];


                switch (action) {
                    case "Deposit":
                        switch (description) {
                            case "RS" -> {
                                String[] rsValues = objectReader.readLine(page.skipNextLine().line());
                                rsuRecords.add(
                                        new RsuRecord(
                                                date,
                                                Integer.parseInt(quantity),
                                                parsePrice(rsValues[4]),
                                                parseDate(rsValues[3])
                                        )
                                );
                            }
                            case "ESPP" -> {
                                String[] esppValues = objectReader.readLine(page.skipNextLine().line());
                                esppRecords.add(
                                        new EsppRecord(
                                                Integer.parseInt(quantity),
                                                parsePrice(esppValues[2]),
                                                parsePrice(esppValues[5]),
                                                parseDate(esppValues[1])
                                        )
                                );
                            }
                            default -> throw new IllegalStateException("Unexpected value: " + description);
                        }

                        break;

                    case "Dividend":
                        dividendRecords.add(
                                new DividendRecord(
                                        date,
                                        parsePrice(amount)
                                )
                        );
                        break;

                    case "Tax Withholding":
                        taxRecords.add(
                                new TaxRecord(
                                        date,
                                        parsePrice(amount)
                                )
                        );
                        break;

                    case "Journal":
                        // Ignore
                        break;

                    case "Sale":
                        saleRecords.addAll(salesParser.parse(page));
                        break;

                    default:
                        throw new IllegalStateException("Unknown report type item " + action);
                }
            }
        } while (!page.nextLine().endOfPage());

        return new ParsedInput(
                rsuRecords,
                esppRecords,
                dividendRecords,
                taxRecords,
                saleRecords
        );
    }

}
