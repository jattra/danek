package jattra.danek.input;

import com.google.common.collect.ImmutableList;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;
import java.util.List;

import static jattra.danek.input.ParsingUtil.parseDate;
import static jattra.danek.input.ParsingUtil.parsePrice;

@RequiredArgsConstructor
public class SalesParser {

    private final CsvReader csvReader;

    public List<SaleRecord> parse(Page page) {
        LocalDate saleDate = parseDate(csvReader.readLine(page)[0]);
        ImmutableList.Builder<SaleRecord> records = ImmutableList.builder();
        String[] line = csvReader.readLine(page.nextLine());

        while (sameSection(line)) {
            if (isData(line)) {
                line = csvReader.readLine(page);
                SaleRecord record = switch (line[1]) {
                    case "RS" -> new SaleRecord(
                            saleDate,
                            Integer.parseInt(line[2]),
                            parsePrice(line[3]),
                            parsePrice(line[12]),
                            parseDate(line[11])
                    );

                    case "ESPP" -> new SaleRecord(
                            saleDate,
                            Integer.parseInt(line[2]),
                            parsePrice(line[3]),
                            parsePrice(line[8]),
                            parseDate(line[6])
                    );

                    default -> throw new IllegalStateException("Unknown Sale type " + line[1]);
                };
                records.add(record);
            }
            line = csvReader.readLine(page.nextLine());
        }

        page.previousLine();
        return records.build();
    }

    private boolean sameSection(String[] line) {
        return line != null && line.length > 0 && line[0].isBlank();
    }

    private boolean isData(String[] line) {
        return line.length > 1 && (line[1].equals("RS") || line[1].equals("ESPP"));
    }
}
