package jattra.danek.input;

import jattra.danek.input.json.JsonInputLoader;
import jattra.danek.input.json.TransactionEntry;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class JsonInputParser {

    private final JsonInputLoader loader = new JsonInputLoader();
    private final RsuJsonParser rsuParser = new RsuJsonParser();
    private final EsppJsonParser esppParser = new EsppJsonParser();
    private final DividendJsonParser dividendParser = new DividendJsonParser();
    private final TaxJsonParser taxParser = new TaxJsonParser();
    private final SaleJsonParser saleParser = new SaleJsonParser();

    public ParsedInput parse(Path inputFile) throws IOException {

        List<RsuRecord> rsuRecords = new ArrayList<>();
        List<EsppRecord> esppRecords = new ArrayList<>();
        List<DividendRecord> dividendRecords = new ArrayList<>();
        List<TaxRecord> taxRecords = new ArrayList<>();
        List<SaleRecord> saleRecords = new ArrayList<>();

        List<TransactionEntry> transactions = loader.load(Files.newInputStream(inputFile));

        for (TransactionEntry t : transactions) {
            switch (TransactionType.from(t)) {
                case RsuDeposit -> rsuRecords.add(rsuParser.parse(t));
                case EsppDeposit -> esppRecords.add(esppParser.parse(t));
                case Dividend -> dividendRecords.add(dividendParser.parse(t));
                case Tax -> taxRecords.add(taxParser.parse(t));
                case Sale -> saleRecords.addAll(saleParser.parse(t));
            }
        }


        return new ParsedInput(
                rsuRecords,
                esppRecords,
                dividendRecords,
                taxRecords,
                saleRecords
        );
    }

    private enum TransactionType {
        RsuDeposit,
        EsppDeposit,
        Dividend,
        Tax,
        Sale,
        Journal;

        static TransactionType from(TransactionEntry t) {
            if (t.action().equals("Deposit") && t.description().equals("RS")) {
                return RsuDeposit;
            } else if (t.action().equals("Deposit") && t.description().equals("ESPP")) {
                return EsppDeposit;
            } else if (t.action().equals("Dividend")) {
                return Dividend;
            } else if (t.action().equals("Tax Withholding")) {
                return Tax;
            } else if (t.action().equals("Sale")) {
                return Sale;
            } else if (t.action().equals("Journal")) {
                return Journal;
            } else {
                throw new IllegalArgumentException("Unknown transaction " + t);
            }
        }
    }
}
