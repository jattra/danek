package jattra.danek.input;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import lombok.RequiredArgsConstructor;
import org.checkerframework.checker.nullness.qual.Nullable;

@RequiredArgsConstructor
public class CsvReader {

    public static final CsvReader SHARED =  create();

    private final ObjectReader reader;


    private static CsvReader create() {
        CsvMapper csvMapper = new CsvMapper();

        CsvSchema schema = csvMapper.schemaFor(String[].class)
                .withColumnSeparator(',')
                .withQuoteChar('"')
                .withoutEscapeChar();

        return new CsvReader(csvMapper.readerFor(String[].class).with(schema));
    }

    @Nullable
    public String[] readLine(Page page) {
        if (page.endOfPage()) {
            return null;
        }
        return readLine(page.line());
    }

    public String[] readLine(String csvLine) {
        try {
            return reader.readValue(csvLine);
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
