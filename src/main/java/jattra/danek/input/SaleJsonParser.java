package jattra.danek.input;

import com.google.common.base.Preconditions;
import jattra.danek.input.json.TransactionDetail;
import jattra.danek.input.json.TransactionEntry;

import java.time.LocalDate;
import java.util.List;

import static jattra.danek.input.ParsingUtil.parseDate;
import static jattra.danek.input.ParsingUtil.parsePrice;

public class SaleJsonParser {

    public List<SaleRecord> parse(TransactionEntry t) {
        Preconditions.checkState(!t.details().isEmpty());
        return t.details().stream()
                .map(TransactionEntry.Inner::detail)
                .map(d -> from(parseDate(t.date()), d))
                .toList();
    }

    private SaleRecord from(LocalDate saleDate, TransactionDetail detail) {
        return switch(detail.type()){
            case "RS" -> new SaleRecord(
                    saleDate,
                    Integer.parseInt(detail.shares()),
                    parsePrice(detail.salePrice()),
                    parsePrice(detail.vestFmv()),
                    parseDate(detail.vestDate())
            );

            case "ESPP" -> new SaleRecord(
                    saleDate,
                    Integer.parseInt(detail.shares()),
                    parsePrice(detail.salePrice()),
                    parsePrice(detail.purchaseFmv()),
                    parseDate(detail.purchaseDate())
            );

            default -> throw new IllegalStateException("Unknown Sale type " + detail.type());
        };

    }
}
