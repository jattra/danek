package jattra.danek.input;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class Page {

    private final List<String> lines;
    @Getter
    private int currentLine = 0;

    public String line() {
        return lines.get(currentLine);
    }

    public Page nextLine() {
        currentLine++;
        return this;
    }

    public void previousLine() {
        currentLine--;
    }

    public Page skipNextLine() {
        currentLine += 2;
        return this;
    }

    public boolean endOfPage() {
        return currentLine >= lines.size() || lines.get(currentLine).isBlank();
    }
}
