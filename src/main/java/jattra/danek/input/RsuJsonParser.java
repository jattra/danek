package jattra.danek.input;

import com.google.common.base.Preconditions;
import jattra.danek.input.json.TransactionDetail;
import jattra.danek.input.json.TransactionEntry;

import static jattra.danek.input.ParsingUtil.parseDate;
import static jattra.danek.input.ParsingUtil.parsePrice;

public class RsuJsonParser {

    public RsuRecord parse(TransactionEntry t) {
        Preconditions.checkState(t.details().size() == 1);
        TransactionDetail detail = t.details().get(0).detail();
        return new RsuRecord(
                parseDate(t.date()),
                Integer.parseInt(t.quantity()),
                parsePrice(detail.vestFmv()),
                parseDate(detail.vestDate())
        );
    }
}
