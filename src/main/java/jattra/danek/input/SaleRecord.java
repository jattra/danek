package jattra.danek.input;

import lombok.Value;

import java.time.LocalDate;

@Value
public class SaleRecord implements HavingDecisiveDate {
    LocalDate saleDate;
    int quantity;
    double salePrice;
    double purchasePrice;
    LocalDate purchaseDate;

    @Override
    public LocalDate getDecisiveDate() {
        return saleDate;
    }
}
