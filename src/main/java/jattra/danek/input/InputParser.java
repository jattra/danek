package jattra.danek.input;

import java.io.IOException;
import java.nio.file.Path;

public class InputParser {

    public ParsedInput parse(Path inputFile) throws IOException {
        if (inputFile.toString().endsWith(".csv")) {
            return new CsvInputParser().parse(inputFile);
        } else if (inputFile.toString().endsWith(".json")) {
            return new JsonInputParser().parse(inputFile);
        } else {
            throw new IllegalArgumentException("Unsupported input file " + inputFile);
        }
    }
}
