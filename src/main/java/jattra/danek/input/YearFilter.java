package jattra.danek.input;

import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class YearFilter {

    private final int year;

    public ParsedInput filterThatYear(ParsedInput input) {
        return new ParsedInput(
                filter(input.getRsuRecords()),
                filter(input.getEsppRecords()),
                filter(input.getDividendRecords()),
                filter(input.getTaxRecords()),
                filter(input.getSaleRecords())
        );
    }

    private <T extends HavingDecisiveDate> List<T> filter(List<T> entries) {
        return entries.stream()
                .filter(this::happenedThatYear)
                .toList();
    }

    private boolean happenedThatYear(HavingDecisiveDate entry) {
        return entry.getDecisiveDate().getYear() == year;
    }
}
