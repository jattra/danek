package jattra.danek.input;

import lombok.Value;

import java.util.List;

@Value
public class ParsedInput {
    List<RsuRecord> rsuRecords;
    List<EsppRecord> esppRecords;
    List<DividendRecord> dividendRecords;
    List<TaxRecord> taxRecords;
    List<SaleRecord> saleRecords;
}
