package jattra.danek.input;

import jattra.danek.input.json.TransactionEntry;

import static jattra.danek.input.ParsingUtil.parseDate;
import static jattra.danek.input.ParsingUtil.parsePrice;

public class DividendJsonParser {

    public DividendRecord parse(TransactionEntry t) {
        return new DividendRecord(
                parseDate(t.date()),
                parsePrice(t.amount())
        );
    }
}
