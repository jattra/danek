package jattra.danek.input;

import lombok.Value;

import java.time.LocalDate;

@Value
public class EsppRecord implements HavingDecisiveDate {
    int quantity;
    double purchasePrice;
    double purchaseFmv;
    LocalDate purchaseDate;

    @Override
    public LocalDate getDecisiveDate() {
        return purchaseDate;
    }
}
