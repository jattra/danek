package jattra.danek.input;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class ParsingUtilTest {

    @Test
    void parseDate() {
        assertThat(
                ParsingUtil.parseDate("06/19/2020"),
                is(LocalDate.of(2020, 6, 19))
        );
    }

    @Test
    void parsePrice() {
        assertThat(
                ParsingUtil.parsePrice("$156.94"),
                is(156.94d)
        );
    }

    @Test
    void parsePriceWithSeparator() {
        assertThat(
                ParsingUtil.parsePrice("$23,189.46"),
                is(23_189.46d)
        );
    }

    @Test
    void parseNegativePrice() {
        assertThat(
                ParsingUtil.parsePrice("-$23.54"),
                is(-23.54d)
        );
    }
}