package jattra.danek.input.json;

import com.google.common.io.Resources;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

import static java.util.Collections.emptyList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class JsonInputLoaderTest {

    private JsonInputLoader loader;

    @BeforeEach
    void setUp() {
        loader = new JsonInputLoader();
    }

    @Test
    void load() throws IOException {
        assertThat(
                loader.load(Resources.getResource(JsonInputLoaderTest.class, "input.json").openStream()),
                is(List.of(
                        new TransactionEntry("01/24/2024", "Tax Withholding", null, "Debit", "-$47.68", emptyList()),
                        new TransactionEntry("01/24/2024", "Dividend", null, "Credit", "$317.85", emptyList()),
                        new TransactionEntry("01/18/2024", "Sale", "167", "Share Sale", "$8,475.15", List.of(
                                new TransactionEntry.Inner(new TransactionDetail("ESPP", "107", "$50.75", "06/30/2023", "$36.21", "$51.74", "", "")),
                                new TransactionEntry.Inner(new TransactionDetail("RS", "19", "$52.325", "", "", "", "12/10/2019", "$43.90"))
                        )),
                        new TransactionEntry("01/02/2024", "Deposit", "121", "ESPP", null, List.of(new TransactionEntry.Inner(new TransactionDetail(null, null, null, "12/29/2023", "$36.21", "$50.52", null, null)))),
                        new TransactionEntry("12/13/2023", "Deposit", "7", "RS", null, List.of(new TransactionEntry.Inner(new TransactionDetail(null, null, null, null, null, null, "12/10/2023", "$48.38"))))
                ))
        );


    }
}