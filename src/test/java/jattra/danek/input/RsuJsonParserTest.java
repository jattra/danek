package jattra.danek.input;

import jattra.danek.input.json.TransactionDetail;
import jattra.danek.input.json.TransactionEntry;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class RsuJsonParserTest {

    private final RsuJsonParser parser = new RsuJsonParser();

    @Test
    void parse() {
        assertThat(
                parser.parse(new TransactionEntry(
                        "09/13/2022",
                        "Deposit",
                        "7",
                        "RS",
                        "",
                        newArrayList(new TransactionEntry.Inner(new TransactionDetail(
                                null, null, null, null, null, null, "12/10/2023", "$48.38"
                        )))
                )),
                is(new RsuRecord(
                                LocalDate.of(2022, 9, 13),
                                7,
                                48.38,
                                LocalDate.of(2023, 12, 10)
                        )
                )
        );
    }
}