package jattra.danek.input;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import jattra.util.ResourceUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class SalesParserTest {


    private SalesParser parser;

    @BeforeEach
    void setUp() {
        parser = new SalesParser(CsvReader.SHARED);
    }

    @Test
    void rsuSalesOnly() {
        List<String> lines = ResourceUtil.loadResource(SalesParser.class, "rsu_sales.csv");
        LocalDate expectedSaleDate = LocalDate.of(2022, 9, 23);
        assertThat(
                parser.parse(new Page(lines)),
                is(ImmutableList.of(
                        new SaleRecord(expectedSaleDate, 7, 41.45, 45.77, LocalDate.of(2022, 9, 10)),
                        new SaleRecord(expectedSaleDate, 6, 41.45, 45.31, LocalDate.of(2022, 6, 11)),
                        new SaleRecord(expectedSaleDate, 3, 41.015, 44.26, LocalDate.of(2021, 3, 9))
                ))
        );
    }

    @Test
    void esppSalesOnly() {
        List<String> lines = ResourceUtil.loadResource(SalesParser.class, "espp_sales.csv");
        assertThat(
                parser.parse(new Page(lines)),
                is(ImmutableList.of(
                        new SaleRecord(LocalDate.of(2022, 11, 22), 40, 48.135, 63.37, LocalDate.of(2021, 12, 31))
                ))
        );
    }

    @Test
    void esppAndRsuSales() {
        List<String> lines = ResourceUtil.loadResource(SalesParser.class, "combo_sales.csv");
        LocalDate expectedSaleDate = LocalDate.of(2022, 11, 28);
        assertThat(
                parser.parse(new Page(lines)),
                is(ImmutableList.of(
                        new SaleRecord(expectedSaleDate, 40, 48.23, 63.37, LocalDate.of(2021, 12, 31)),
                        new SaleRecord(expectedSaleDate, 19, 48.21, 44.26, LocalDate.of(2021, 9, 10)),
                        new SaleRecord(expectedSaleDate, 20, 48.20, 52.78, LocalDate.of(2021, 6, 30))
                ))
        );
    }

    @Test
    void salesAndThenSomethingElse() {
        List<String> lines = ImmutableList.copyOf(Iterables.concat(
                ResourceUtil.loadResource(SalesParser.class, "espp_sales.csv"),
                ResourceUtil.loadResource(SalesParser.class, "rsu_deposit.csv")
        ));
        assertThat(
                parser.parse(new Page(lines)),
                is(ImmutableList.of(
                        new SaleRecord(LocalDate.of(2022, 11, 22), 40, 48.135, 63.37, LocalDate.of(2021, 12, 31))
                ))
        );
    }
}