package jattra.danek.compute;

import jattra.danek.input.SaleRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class SalesMergerTest {

    private SalesMerger merger;

    @BeforeEach
    void setUp() {
        merger = new SalesMerger();
    }

    @Test
    void merge() {
        assertThat(Set.copyOf(
                        merger.merge(List.of(
                                new SaleRecord(LocalDate.of(2020, 3, 24), 10, 10.20, 11.40, LocalDate.of(2020, 2, 18)),
                                new SaleRecord(LocalDate.of(2020, 3, 24), 11, 21.35, 11.40, LocalDate.of(2020, 2, 18)),
                                new SaleRecord(LocalDate.of(2020, 3, 24), 21, 21.35, 11.40, LocalDate.of(2020, 2, 18)),
                                new SaleRecord(LocalDate.of(2020, 3, 24), 33, 21.35, 13.84, LocalDate.of(2020, 2, 18)),
                                new SaleRecord(LocalDate.of(2020, 3, 24), 55, 21.35, 13.84, LocalDate.of(2020, 2, 19)),
                                new SaleRecord(LocalDate.of(2020, 3, 23), 89, 10.20, 11.40, LocalDate.of(2020, 2, 18)),
                                new SaleRecord(LocalDate.of(2020, 3, 23), 14, 10.20, 11.40, LocalDate.of(2020, 2, 18))
                        ))),
                is(Set.of(
                        new SaleRecord(LocalDate.of(2020, 3, 24), 10, 10.20, 11.40, LocalDate.of(2020, 2, 18)),
                        new SaleRecord(LocalDate.of(2020, 3, 24), 32, 21.35, 11.40, LocalDate.of(2020, 2, 18)),
                        new SaleRecord(LocalDate.of(2020, 3, 24), 33, 21.35, 13.84, LocalDate.of(2020, 2, 18)),
                        new SaleRecord(LocalDate.of(2020, 3, 24), 55, 21.35, 13.84, LocalDate.of(2020, 2, 19)),
                        new SaleRecord(LocalDate.of(2020, 3, 23), 103, 10.20, 11.40, LocalDate.of(2020, 2, 18))
                ))
        );
    }
}