# Daněk
Nastroj pro zpracovani exportu ze Schwaba. Vypise agregovane tabulky s informacemi 
o vestingu, zisku z prodeje a dividend. Vystup lze snadno vlozit do tabulkoveho
nastroje jako Google Doc a pouzit pro ucely danoveho priznani. 

## Navod
Aktualizuj YearConstantExchangeRateProvider
Zajisti vstup, spust, nakopiruj vystup do tabulky, zformatuj a exportuj dle libosti.

### Argumenty
1. Cesta k exportu ze Schwaba.
2. Rok, ze ktereho jsou exportovana data.

### Export ze Schwaba
Equity Awards -> View Transaction History
Exportuj Custom range: 1st Dec pre_year - 1st Feb after_year, abys zahrnul pohyby na rozhrani roku (hlavne kvuli ESPP).
Uloz JSON a odkaz se na nej v prvnim parametru aplikace.

### Vystup
Vystup je v textove forme. Copy/paste do Google Doc tabulek. Exportuj jako PDF.

### Info k DAP
RSU a ESPP jsou příjmy podle §6 (od zaměstnavatele) jedno kde vznikly, takže se zahrnují do tabulky k řádku 31: Úhrn příjmů od všech zaměstnavatelů
Dividendy jsou příjmy podle §8 (z kapitálového majetku) jedno kde vznikly, takže se zahrnují do řádku 38: Dílčí základ daně z kapitálového majetku podle § 8 zákona (není k tomu speciální tabulka)

Daň odvedená v zahraničí se uvádí do přílohy 3 a to tak (pokud se použije metoda prostého zápočtu), že:
•	do řádku 321 se znovu opíše zisk v zahraničí (tedy nejspíš to samé číslo jako je na řádku 38)
•	do řádku 323 se vyplní daň zaplacená v zahraničí
(nelekněte se jako já, řádek 330 pak obsahuje celou daň z příjmu s odečtenou částkou daně zaplacenou v zahraničí)

Prodej akcií je příjem podle §10 (ostatní příjmy) a uvádí se do přílohy 2 DAP, odkud se propisují do řádku 40: Dílčí základ daně z ostatních příjmů podle § 10 zákona (ř. 209 přílohy č. 2 DAP)